# Elastic Stack - Juin 2020

Resources:
* [spy-log-bot](https://gitlab.com/formation-pro1/misc/spy-log-bot) 
* [Java en version 11 minimum](https://adoptopenjdk.net/)
* [Elasticsearch](https://www.elastic.co/fr/downloads/elasticsearch)
* [Logstash](https://www.elastic.co/fr/downloads/logstash)
* [Kibana](https://www.elastic.co/fr/downloads/kibana)
